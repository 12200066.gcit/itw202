import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { MyComponent } from './components/MyComponent';
export default function App() {
  return (
    <View style={styles.container}>
      <MyComponent></MyComponent>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: ' center',
    justifyContent: 'center',
  },
});
