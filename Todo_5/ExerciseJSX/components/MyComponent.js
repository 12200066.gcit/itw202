import React from 'react';
import {View,StyleSheet,Text} from  'react-native';

const yourname='Sonam Wangmo';
const MyComponent=()=>{
    return (
        <View>
            <Text style={styles.textstyle}>Getting started with React Native</Text>
            <Text style={styles.textstyle1}>My name is {yourname}</Text>
        </View>
    )
};

const styles=StyleSheet.create({
    textstyle:{
        fontSize:45,
    },
    textstyle1:{
        fontSize:10,
    }
});
export {MyComponent};
