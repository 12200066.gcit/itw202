import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View ,ScrollView} from 'react-native';
import MyButtonExample from './components/ButtonExample';
import CustomComponents from './components/CustomComponents';
import MyTouchableHightExample from './components/TouchableHIghlight';
import TouchableNativeFeedbackExample from './components/TouchableNativeFeedback'

import TouchableOpacityExample from './components/TouchableOpacity';
export default function App() {
  return (
  <ScrollView>
       <><MyButtonExample></MyButtonExample>
      <MyTouchableHightExample></MyTouchableHightExample></>
      <TouchableNativeFeedbackExample></TouchableNativeFeedbackExample>
      <TouchableOpacityExample></TouchableOpacityExample>
    <CustomComponents></CustomComponents>
    </ScrollView>
   
    
  );
}


