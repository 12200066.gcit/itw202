import React from 'react';
import { TouchableOpacity,Text,View,StyleSheet } from 'react-native';
import { COlORS } from './constants/colors';
import RowItem from './RowItems'
function CustomComponents(){
    return(
        <View style={styles.container}>
            {/* <TouchableOpacity style={styles.row}>
                <Text>Theme</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.row}>
                <Text>React Native Basics</Text>
            </TouchableOpacity>
             <TouchableOpacity style={styles.row}>
                <Text>React Native by Example</Text>
            </TouchableOpacity> */}
            <RowItem text='Themes'/>
            <RowItem text='React native basics'/>
            <RowItem text='React native by example'/>
        </View>
    )
};
const styles =StyleSheet.create({
    container:{
        flex:1,
        marginTop:30,
        marginLeft:90,
    },
    });
export default CustomComponents;