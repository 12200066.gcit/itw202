import react, { useState }from "react"; 
import {View, Text,TouchableOpacity,StyleSheet} from 'react-native';

const TouchableOpacityExample =() =>{
    const [count , setCount] =useState(0);
    return (
        <View style={{padding:100}}>
            <Text>You clicked {count} times</Text>
            <TouchableOpacity onPress={()=> setCount(count+1)} style={styles.button}>
                <Text>Count</Text>
            </TouchableOpacity>
        </View>
    )
};

const styles=StyleSheet.create({
    button:{
        alignItems:'center',
        backgroundColor:'#DDDDDD',
        padding:10,
    }
});
export default TouchableOpacityExample;