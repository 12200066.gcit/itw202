import react from "react";
import {TouchableOpacity,Text,StyleSheet, View} from 'react-native';
import { COlORS } from "./constants/colors";

const RowItem =({text}) =>{
    return(
        <TouchableOpacity style={styles.row}>
            <Text style={styles.text}>{text}</Text>
        </TouchableOpacity>
    )
};
const styles =StyleSheet.create({
    row:{
    paddingHorizontal:20,
    paddingVertical:16,
    justifyContent:'space-between',
    alignItems:'center',
    flexDirection:'row',
    backgroundColor: COlORS.white,
    },
    title:{
        color:COlORS.text,
        fontSize:16,
    }
});
export default RowItem;