import react, {useState} from "react";
import {View, Text, Button,StyleSheet} from 'react-native';

const MyButtonExample =() =>{
    const [count,setCount] =useState(0);
    return(
        <View style={styles.container}>
            <Text>You clicked {count} times</Text>
            <Button onPress={()=> setCount(count+1)} title='Count'></Button>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
      marginTop:50,
    },
  });
export default MyButtonExample;