import React ,{useState} from "react";
import { StyleSheet,Text, TouchableHighlight,View } from "react-native";

const MyTouchableHightExample =() =>{
    const [count, setCount] =useState(0);
    const onPress =()=>setCount(count+1);

    return(
        <View style={{padding:100}}> 
            <Text>You clicked {count} times</Text>
            <TouchableHighlight onPress={onPress} style={styles.button}>
                <Text>Count</Text>
            </TouchableHighlight>
           
        </View>
    )
};
const styles =StyleSheet.create({
    button:{
        alignItems:'center',
        backgroundColor:'#DDDDDD',
        padding:10,
    }
});
export default MyTouchableHightExample;