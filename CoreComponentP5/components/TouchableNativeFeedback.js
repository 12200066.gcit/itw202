import React,{useState} from 'react';
import {View , Text , StyleSheet, TouchableNativeFeedback} from 'react-native';

const TouchableNativeFeedbackExample =()=>{
    const [count, setCount] = useState();
    return(
        <View style={{padding:100}}>
            <Text>You clicked {count} times</Text>
            <TouchableNativeFeedback style={StyleSheet.button} onPress={()=>setCount(count+1) }>
                <Text>Count</Text>
            </TouchableNativeFeedback>
        </View>
    )
};
const styles=StyleSheet.create({
    button:{
        alignItems:'center',
        backgroundColor:'#DDDDDD',
        padding:10,
    }
});
export default TouchableNativeFeedbackExample;