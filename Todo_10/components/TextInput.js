import react  from "react";
import {useState} from 'react'
import {Text,View,TextInput, StyleSheet} from 'react-native'


const myTextInput=()=>{
    const [text,setText]=useState("_____");
    return(
        <View style={styles.container}>
            <Text>Hello{text} Have a good day!</Text>
            <TextInput secureTextEntry placeholder="Enter your Name" onChangeText={(val)=>setText(val)}/>
        </View>
    )
};
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });
export default myTextInput;