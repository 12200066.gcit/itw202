import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import AllPlaces from './Screen/AllPlaces';
import AddPlace from './Screen/AddPlace';
import IconButton from './component/ui/IconButton';
import { Color } from './component/constants/Color';
const Stack=createNativeStackNavigator()
export default function App() {
  return (
     <>

     <StatusBar style='dark'/>
     <NavigationContainer>
     <Stack.Navigator
     screenOptions={{
       headerStyle:{backgroundColor:Color.primary500},
       heaaderTintColor:Color.gray700,
       contentStyle:{backgroundColor:Color.gray700}
     }}>
     <Stack.Screen name='AllPlaces'
       component={AllPlaces}
       options={({navigation})=>({
         title:'Your Favourite Places',
         headerRight:({tintColor})=>(
           <IconButton 
             icon='add'
             size={24}
             color={tintColor}
             onPress={()=>navigation.navigate('AddPlaces')}
           />
         ),
       })}
     />

       <Stack.Screen name='AddPlaces' component={AddPlace}/>
       </Stack.Navigator>
     </NavigationContainer>
   </> 
  ); 
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
