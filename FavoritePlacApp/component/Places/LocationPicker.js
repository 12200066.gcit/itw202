import { Alert, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import OutlineButton from '../ui/OutlineButton'
import { Color } from '../constants/Color'
import {getCurrentPositionAsync,useForegroundPermissions,PermissionStatus} from 'expo-location'
const LocationPicker = () => {
    const [locationPermissionInformation ,requestPermission]=useForegroundPermissions();
    async function VerfyPermissions(){
        if(
            locationPermissionInformation.status === PermissionStatus.UNDETERMINED
        ){
            const permissionResponse =await requestPermission();

            return permissionResponse.granted;
        }

        if(locationPermissionInformation.status ===PermissionStatus.DENIED){
            Alert.alert(
                'Insufficient Permission',
                'You need to grant location permission to use this app'
            );
            return false;
        }
        return true;
    }
     async function getLocationHandler(){
        // async function getLocationHandler(){
        //     const location =await getCurrentPositionAsync();
        //     console.log(location)
        // }
        const hasPermission =await VerfyPermissions();
        if (!hasPermission){
            return;
        }
        const location =await getCurrentPositionAsync();
        console.log(location)
    }
    function getOnMapHandler(){

    }
  return (
    <View>
     <View style={styles.mapPreview}></View>
    <View style={styles.action}>
        <OutlineButton icon="location" onPress={getLocationHandler}>
            Locate User
        </OutlineButton>
        <OutlineButton icon="map" onPress={getOnMapHandler}>
            Pick on Map 
        </OutlineButton>
    </View>
    </View>
  )
}

export default LocationPicker

const styles = StyleSheet.create({
    mapPreview:{
        width:'100%',
        height:200,
        marginVertical:8,
        alignItems:'center',
        backgroundColor:Color.primary100,
        borderRadius:4,
    },
    action:{
        flexDirection:'row',
        justifyContent:'space-around',
        alignItems:'center',
    }
})