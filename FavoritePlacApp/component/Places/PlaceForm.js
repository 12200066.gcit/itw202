import { ScrollView, StyleSheet, Text, TextInput, View } from 'react-native'
import React, { useState } from 'react'
import { Color } from '../constants/Color'
import ImagePicker from './ImagePicker'
import LocationPicker from './LocationPicker'


const PlaceForm = () => {
    const [enteredTitle,setEnteredTitle]=useState('')
    function ChangeTittleHandler(enteredText){
        setEnteredTitle(enteredText)
    }
  return (
      <ScrollView style={styles.form}>
    <View>
      <Text style={styles.label}>PlaceForm</Text>
      <TextInput
          style={styles.input}
          onChangeText={ChangeTittleHandler}
          value={enteredTitle}
      />
    </View>
    <ImagePicker/>
    <LocationPicker/>
    </ScrollView>
  )
}

export default PlaceForm

const styles = StyleSheet.create({
    form:{
    flex:1,
    padding:24,
    },
    label:{
        fontWeight:'bold',
        marginBottom:4,
        color:Color.primary500,

    },
    input:{
       marginVertical:8,
       paddingHorizontal:4,
       paddingVertical:8,
       fontSize:16,
       borderBottomColor:Color.primary700,
       borderBottomWidth:2,
       backgroundColor:Color.primary100, 

    }
})