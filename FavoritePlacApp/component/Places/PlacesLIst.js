import { FlatList, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import PlaceItem from './PlaceItem'
import { Color } from '../constants/Color'

const PlacesList = ({places}) => {
  if(!places || places.length==0){
    return(<View style={styles.fallbackContainer}>
      <Text style={styles.fallbackText}>No place added yet -Start Adding some!</Text>
    </View>)
  }
  return (
  <FlatList
    data={places}
    keyExtractor={(item)=>item.id}
    renderItem={PlaceItem}
  />
  )
}

export default PlacesList

const styles = StyleSheet.create({
  fallbackContainer:{
    flex:1,
    justifyContent:'center',
    alignItems:'center',
  },
  fallbackText:{
    fontSize:16,
    color:Color.primary200,
  }
})