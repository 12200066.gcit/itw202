import { Alert, Button, StyleSheet, Text, View,Image } from 'react-native'
import React,{useState} from 'react'
import {
    launchCameraAsync,
    useCameraPermissions,
    PermissionStatus
} from 'expo-image-picker'
import { Color } from '../constants/Color'
import OutlineButton from '../ui/OutlineButton'

const ImagePicker = () => {
    const [pickImage,setPickedImage]=useState()
    const [camerPermissonInformation,requestPermission]=useCameraPermissions();
    async function VerfyPermissions(){
        if(camerPermissonInformation.status ===PermissionStatus.UNDETERMINED){
            const permissionResponse =await requestPermission();

            return permissionResponse.granted;
        }
        if (camerPermissonInformation.status===PermissionStatus.DENIED){
            Alert.alert(
                'Insufficient Persmissions!',
                'You need to grant camer permission to use this app'
            );
            return false;
        }
        return true
    }   
    async function takeImageHandler(){
        const hasPermission =await VerfyPermissions();
        if (!hasPermission){
            return;
        }
    const image =await launchCameraAsync({
        allowEditing:true,
        aspect:[16,9],
        quality:0.5,
    }
    );
    console.log(image) 
    setPickedImage(image.uri);  
}
let imagePreview=<Text>No image taken yet.</Text>
if (pickImage){
    imagePreview=<Image styles={styles.image} source={{uri: pickImage}}/>
}
  return (
    <View>
    <View style={styles.imagePreview}>{imagePreview}</View>
    {/* <Button title='Take image' onPress={takeImageHandler}/> */}
    <OutlineButton icon='camera' onPress={takeImageHandler}>Take Image</OutlineButton>
    </View>
  )
}

export default ImagePicker

const styles = StyleSheet.create({
    imagePreview:{
        width:'100%',
        height:200,
        marginVertical:8,
        alignItems:'center',
        backgroundColor:Color.primary100,
        borderRadius:4,
    },
    image:{
        width:'100%',
        height:"100%"
    }
})