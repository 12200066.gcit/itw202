import react from "react";
import {Image ,View,StyleSheet,Text} from 'react-native';

const MyImagecomponet=()=>{
    return(
        <View style={styles.container}>
        <Image source={{uri: 'https://picsum.photos/100/100'}} style={styles.myimage}/>
        <Image source={require('../assets/reactnativelogo.png')}></Image>
        </View>
     
        )
};
const styles = StyleSheet.create({
    myimage:{
        width:100,
        height:100,
    },
    container:{
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    }
   
})
export default MyImagecomponet;