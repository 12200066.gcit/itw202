import React from 'react';
import {Image,StyleSheet} from 'react-native';

export default function Logo(){
    return <Image source={require('../../assets/reactivelogo.png')} style={styles.image}/>
}
const styles=StyleSheet.create({
    image:{
        width:115,
        height:115,
        marginBottom:8,
        marginTop:"50%",
      

    },
})