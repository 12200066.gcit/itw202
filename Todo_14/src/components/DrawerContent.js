import React from 'react'
import {View} from 'react-native'
import {Text} from 'react-native-paper'
import {DrawerItem,DrawerContentScrollView} from '@react-navigation/drawer';
import { Avatar} from 'react-native-paper';
import { TouchableOpacity } from 'react-native';
import { Switch } from 'react-native';

export default function DrawerContent(){
    return(
        <DrawerContentScrollView>
      <View style={{marginLeft:10}}>
        <View>
          <Avatar.Image source={require('../../assets/icon.png')} size={100}/>
        </View>
        <Text style={{fontSize:20,fontWeight:'bold'}}>Namgyal</Text>
        <Text>Namgyalwangchuk</Text>
      </View>
      <View style={{flexDirection:'row',marginTop:10,marginLeft:10}}>
        <Text><Text style={{fontSize:15,fontWeight:'bold'}}>55</Text>Following</Text>
        <Text style={{marginLeft:20}}><Text style={{fontSize:15,fontWeight:'bold'}}>100</Text>Follower</Text>
      </View>
      <View>
        <TouchableOpacity style={{marginTop:20}}>
             <DrawerItem style={{fontSize:25}} label='Setting'
             onPress={()=>{ }}/>

        </TouchableOpacity>
        <TouchableOpacity>
        <DrawerItem  label='Preference' style={{fontSize:20}} onPress={()=>{ }}/>
        </TouchableOpacity>

        <View style={{marginLeft:10,flexDirection:'row'}}>
          <Text style={{fontSize:25}}>Notifications</Text>
          <Switch style={{marginLeft:50}} value={false}/>
        </View>
      
      </View>
    </DrawerContentScrollView>
   
  )
}

