import React from 'react'
import { ActivityIndicator } from 'react-native'
import Background from '../components/Background'
import firebase from 'firebase/app'
export default function AuthLoadingScreen({navigation}){
    firebase.auth().onAuthStateChanged((user)=>{
        if (user){
            //user is logged in 
            navigation.reset({
                routes:[{name:'HomeScreen'}],
            })

        }
        else{
            //User is not logged in 
            navigation.reset({
                routes:[{name:'StartScreen'}],
            });
        }
    });
 
    return(
     <Background>
         <ActivityIndicator size='large'/>
     </Background>
 )
}