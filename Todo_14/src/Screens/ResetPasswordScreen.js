import React ,{useState}from "react";
import {View,StyleSheet,Text,TouchableOpacity} from 'react-native';
import Background from "../components/Background";
import Button from "../components/Button";
import Header from '../components/Header';
import Paragraph from "../components/Paragraph";
import Logo from "../components/Login";
import TextInput from "../components/TextInput"
import { emailValidator } from "../core/helpers/emailValidator";
import { passwordValidator } from "../core/helpers/passwordValidator";
import BackButton from "../components/BackButton";
import { theme } from "../core/theme";

export default function ResetPasswordScreen({navigation})
{
    const [email,setEmail]=useState({value:"",error:""})
   
    const onSubmitPressed=()=>{
        const emailError=emailValidator(email.value);
        if (emailError){
            setEmail({...email, error: emailError})
            
        }
    }
    return (
        <Background>
            <BackButton goBack={navigation.goBack}/>
            <Logo/>
            <Header>Restore Password</Header>
         
            <TextInput label="Email" 
                value={email.value}
                error={email.error}
                errorText={email.error}
                onChangeText={(text)=>setEmail({value:text,error:""})}
                description="You will recieve email with password reset link" 
            />
        <Button mode='contained' onPress={onSubmitPressed}>Send Instruction</Button>
        </Background>
    )
}
