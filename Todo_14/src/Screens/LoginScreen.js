import react ,{useState}from "react";
import {View,StyleSheet,Text} from 'react-native';
import { TouchableOpacity } from "react-native-gesture-handler";
import Background from "../components/Background";
import Button from "../components/Button";
import Header from '../components/Header';
import Paragraph from "../components/Paragraph";
import Logo from "../components/Login";
import TextInput from "../components/TextInput"
import { emailValidator } from "../core/helpers/emailValidator";
import { passwordValidator } from "../core/helpers/passwordValidator";
import BackButton from "../components/BackButton";
import { theme } from "../core/theme";
import { loginUser } from "../api/auth-api";
export default function LoginScreen({navigation})
{
    const [email,setEmail]=useState({value:"",error:""})
    const [password,setpassword]=useState({value:"",error:""})
    const [loading,setloading]=useState();

    const onLoginPressed=async()=>{
        const emailError=emailValidator(email.value);
        const passwordError =passwordValidator(password.value)
        if (emailError || passwordError){
            setEmail({...email, error: emailError})
            setpassword({...password,error:passwordError})
        }
        setloading(true)
        const response=await loginUser({
            email:email.value,
            password:password.value,
        });
        if(response.error){
            alert(response.error);
        }
        else{
            alert(response.user.displayName)
        }
        setloading(false)
        // else{
        //     navigation.navigate("HomeScreen")
        // }
    }
    return (
        <Background>
            <BackButton goBack={navigation.goBack}/>
            <Logo/>
            <Header>Welcome</Header>
         
            <TextInput label="Email" 
                value={email.value}
                error={email.error}
                errorText={email.error}
                onChangeText={(text)=>{setEmail({value:text,error:""})}}
            />
           <TextInput
           value={password.value}
           error={password.error}
           errorText={password.error}
           onChangeText={(text) => setpassword({value:text,error:""}) } 
           label="Password"
           secureTextEntry
           />
           <View style={styles.forgotpassword}>
               <TouchableOpacity onPress={() => navigation.navigate("ResetPasswordScreen")}>
                   <Text style={styles.forgot}>forgot your password?</Text>
               </TouchableOpacity>
           </View>
            <Button mode='contained' onPress={onLoginPressed}>Login</Button>
            <View style={styles.row}>
                <Text>Don't have an account?</Text>
                <TouchableOpacity  onPress={()=> navigation.replace("RegisterScreen")} 
                >
                      <Text style={styles.link}>Sign Up</Text>
                </TouchableOpacity>
              
            </View>
        </Background>
    )
}
const styles=StyleSheet.create({
    row:{
        flexDirection:'row',
        marginTop:4,
    },
    link:{
        fontWeight:'bold',
        color:theme.colors.primary,
    },
    forgotpassword:{
        width:'100%',
        alignItems:'flex-end',
        marginBottom:24,
    },
    forgot:{
        fontSize:13,
        color:theme.colors.primary,
    }
})