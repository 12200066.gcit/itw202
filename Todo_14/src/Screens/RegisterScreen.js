import react ,{useState}from "react";
import {View,StyleSheet,Text} from 'react-native';
import { TouchableOpacity } from "react-native-gesture-handler";
import Background from "../components/Background";
import Button from "../components/Button";
import Header from '../components/Header';
import Paragraph from "../components/Paragraph";
import Logo from "../components/Login";
import TextInput from "../components/TextInput"
import { emailValidator } from "../core/helpers/emailValidator";
import { passwordValidator } from "../core/helpers/passwordValidator";
import { nameValidator } from "../core/helpers/nameValidator";
import BackButton from "../components/BackButton";
import { theme } from "../core/theme";
import { signUpUser } from "../api/auth-api";
import { set } from "react-native-reanimated";
export default function RegisterScreen({navigation})
{
    const [email,setEmail]=useState({value:"",error:""})
    const [password,setpassword]=useState({value:"",error:""})
    const [name,setname]=useState({value:'',error:''})
    const [loading,setloading] =useState();
    const onSignUpPressed=async()=>{
        const nameError=nameValidator(name.value)
        const emailError=emailValidator(email.value);
        const passwordError =passwordValidator(password.value)
        if (emailError || passwordError || nameError){
            setname({...name,error:nameError})
            setEmail({...email, error: emailError})
            setpassword({...password,error:passwordError})

        }
        setloading(true)
        const response =await signUpUser({
            name:name.value,
            email:email.value,
            password:password.value
        })
        if (response.error){
            alert(response.error)
        }
        else{
            console.log(response.user.displayName)
            alert(response.user.displayName)
        }
        setloading(false)
    }
    return (
        <Background>
            <BackButton goBack={navigation.goBack}/>
            <Logo/>
            <Header>Create Account</Header>
            <TextInput
                value={name.value}
                error={name.error}
                errorText={name.error}
                onChangeText={(text)=>setname({value:text,error:''})}
                label='Name'
            />
            <TextInput label="Email" 
                value={email.value}
                error={email.error}
                errorText={email.error}
                onChangeText={(text)=>{setEmail({value:text,error:""})}}
            />
           <TextInput
           value={password.value}
           error={password.error}
           errorText={password.error}
           onChangeText={(text) => setpassword({value:text,error:""}) } 
           label="Password"
           secureTextEntry
           />
            <Button  loading={loading}mode='contained' onPress={onSignUpPressed}>
            SignUp</Button>
            <View style={styles.row}>
                <Text>Already have an account?</Text>
                <TouchableOpacity  onPress={()=>navigation.replace("LoginScreen")}>
                      <Text style={styles.link}>Login</Text>
                </TouchableOpacity>
              
            </View>
        </Background>
    )
}
const styles=StyleSheet.create({
    row:{
        flexDirection:'row',
        marginTop:4,
    },
    link:{
        fontWeight:'bold',
        color:theme.colors.primary
    }
})