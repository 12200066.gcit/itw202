import react from "react";
import {View,StyleSheet} from 'react-native';
import Background from "../components/Background";
import Button from "../components/Button";
import Header from '../components/Header';
import Paragraph from "../components/Paragraph";
import Logo from "../components/Login";

export default function StartScreen({navigation}){
    return(
        <Background>
        <Logo/>
        <View style={styles.container}>
            <Header>Login Template</Header>
         <Paragraph>
             The eaisest way to start with your amzazing application
         </Paragraph>
         <Button mode='outlined' 
         onPress={()=>{
            navigation.navigate("LoginScreen")}} 
         >
         Login
         </Button>
         <Button mode='contained'
          onPress={()=>{
            navigation.navigate("RegisterScreen")}} 
         >Sign up</Button>
        </View>
            
        </Background>
    
    
    )
}
const styles=StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#fff',
        alignItems:'center',
        justifyContent:'center',
        width:'100%',
    }
})