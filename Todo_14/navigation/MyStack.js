import React from 'react'
import {View,Text} from 'react-native'
import 'react-native-gesture-handler'
import { RegisterScreen,LoginScreen,StartScreen,ResetPasswordScreen } from '../src/Screens';

import { createNativeStackNavigator } from '@react-navigation/native-stack';
import MyDrawer from './MyDrawer';

const Stack=createNativeStackNavigator();

export default function MyStack(){
    return(
        <Stack.Navigator
        initalRouteName="StartScreen"
        screenOptions={{headerShown:false}}
        >
        <Stack.Screen name='StartScreen' component={StartScreen}/>
        <Stack.Screen name='LoginScreen' component={LoginScreen}/>
        <Stack.Screen name='RegisterScreen' component={RegisterScreen}/> 
        <Stack.Screen name='ResetPasswordScreen' component={ResetPasswordScreen}/>       
        <Stack.Screen name='HomeScreen' component={MyDrawer}/>       
      </Stack.Navigator>
    )
}