import React from 'react'
import {View} from 'react-native'
import 'react-native-gesture-handler'
import HomeScreen from '../src/Screens/HomeScreen'
import ProfileScreen from '../src/Screens/ProfileScreen'
import { createDrawerNavigator } from '@react-navigation/drawer';
import LowNavigation from './MyTab'

const Drawer=createDrawerNavigator();

export default function MyDrawer(){
    return(
        <Drawer.Navigator>
        <Drawer.Screen name='Home' component={LowNavigation}/>
        <Drawer.Screen name='Profile' component={ProfileScreen}/>
        </Drawer.Navigator>
    )
}