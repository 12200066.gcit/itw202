import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View ,Image} from 'react-native';
import {BottomNavigation, Provider} from 'react-native-paper';
import { theme } from './src/core/theme';
import Button from './src/components/Button';
// import TextInput from './src/components/TextInput';
// import Header from './src/components/Header';
import { NavigationContainer, StackActions } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { StartScreen,
      LoginScreen,
      RegisterScreen,
      ResetPasswordScreen,
      HomeScreen,
      ProfileScreen,
      AuthLoadingScreen
} from './src/Screens';
import MyStack from './navigation/MyStack';
import DrawerContent from './src/components/DrawerContent';
import firebase from 'firebase';
import { firebaseConfig } from './src/core/config';
if(!firebase.apps.length){
  firebase.initializeApp(firebaseConfig)
}


const Stack=createNativeStackNavigator();
const Tab=createBottomTabNavigator();
const Drawer=createDrawerNavigator();

export default function App() {
  return (
    <Provider theme={theme}>
      {/* <View style={styles.container}>
        <Text>Open</Text>
        <Header>open</Header>
        <Button mode='contained'>Click me</Button>
        <TextInput label='Email'/>
      </View> */}
      <NavigationContainer>
        <Stack.Navigator
        initalRouteName="AuthLoadingScreen"
        screenOptions={{headerShown:false}}
        >
        <Stack.Screen name='AuthLoadinfScreen' component={AuthLoadingScreen}/>
        <Stack.Screen name='StartScreen' component={StartScreen}/>
        <Stack.Screen name='LoginScreen' component={LoginScreen}/>
        <Stack.Screen name='RegisterScreen' component={RegisterScreen}/> 
        <Stack.Screen name='ResetPasswordScreen' component={ResetPasswordScreen}/>       
        <Stack.Screen name='HomeScreen' component={DrawerNavigator}/>       
      </Stack.Navigator>
      </NavigationContainer>
 
    </Provider>
    
    // <Provider>
    //   <NavigationContainer>
    //     <MyStack/>
    //   </NavigationContainer>
    // </Provider>
    
  );
}
function LowNavigation(){
  return(
    <Tab.Navigator>
      <Tab.Screen name='Home' component={HomeScreen}
        options={{
          tabBarIcon:({size})=>{
            return(
              <Image
                style={{width:size,height:size}}
                source={require('./assets/home-icon.png')}
              />
            )
          }
        }}
      />
      <Tab.Screen name='Profile' component={ProfileScreen}
         options={{
          tabBarIcon:({size})=>{
            return(
              <Image
                style={{width:size,height:size}}
                source={require('./assets/setting-icon.png')}
              />
            )
          }
        }}
      />
    </Tab.Navigator>
  )
}

const DrawerNavigator=()=>{
  return(
    <Drawer.Navigator drawerContent={DrawerContent}>
      <Drawer.Screen name='Home' component={LowNavigation} />
    </Drawer.Navigator>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
