import { firebase } from '@firebase/app'

const CONFIG = {
  apiKey: "AIzaSyDpKMb6_dsLww4pNo6c221XkS2Q6I7gzCE",
  authDomain: "todo-crud-145ac.firebaseapp.com",
  databaseURL: "https://todo-crud-145ac-default-rtdb.firebaseio.com",
  projectId: "todo-crud-145ac",
  storageBucket: "todo-crud-145ac.appspot.com",
  messagingSenderId: "517054398797",
  appId: "1:517054398797:web:3aab9d5455587cdb70b704"
}
firebase.initializeApp(CONFIG)

export default firebase;
