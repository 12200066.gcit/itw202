import react,{useState} from "react";
import {StyleSheet ,View ,TextInput,Button,Modal} from 'react-native';

const AspirationInput=(props)=>{
    const [enteredAspiration,setAspiration]=useState('');
  
    const clearInput = (enteredText)=>{
      setAspiration();
    }
    const AspirationHandler=(enteredText)=>{
      setAspiration(enteredText)
    }
    
    return(      
        <Modal visible={props.visible} animationType='slide'>
             <View style={styles.inputContainer}>
            <TextInput
             placeholder='My Aspiration from this module' 
             style={styles.input} onChangeText={AspirationHandler} 
             value={enteredAspiration} />
            </View>
            <Button title='Add' onPress={() => props.onAddAspiration(enteredAspiration)} />
            <Button title='Cancel' onPress={clearInput}></Button>
        </Modal> 
   
     
    
    )
};
const styles = StyleSheet.create({
    inputContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:"center",
        marginTop:70,
        marginLeft:10,
        
    },
    input:{
        width:'80%',
        borderColor:'black',
        borderWidth:1,
        padding:10,
        marginBottom:10,
      },
      Screen:{
        padding:50
      },
});
export default AspirationInput;