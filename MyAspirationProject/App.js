import { setStatusBarBackgroundColor } from 'expo-status-bar';
import react ,{useState} from 'react';
import { Button,StyleSheet, TextInput, View ,Text,FlatList} from 'react-native';
import AspirationInput from './components/Aspirationinput';
import { AspirationItem } from './components/AspirationItem';



export default function App() {
  // const [enteredAspiration,setAspiration]=useState('');
  const [courseAspiration , setCourseAspiration]=useState([]);
  const [isAddMode,setIsAddMode]=useState(false);

  // const AspirationHandler=(enteredText)=>{
  //   setAspiration(enteredText)
  // }
  const addAspirationHandler= aspirationtitle =>{
    // console.log(enteredAspiration);
    // setCourseAspiration([...courseAspiration,enteredAspiration]);
    // setCourseAspiration(currentAspiration=>[...courseAspiration,enteredAspiration])
    setCourseAspiration(currentAspiration =>[...currentAspiration,{key: Math.random().toString(),value:aspirationtitle}])
    setIsAddMode(false)
  }
  const removeApsirationHandler=aspirationkey=>{
    setCourseAspiration(currentAspiration=>aspirationkey!==aspirationkey)
  }
  return (
    <View style={styles.Screen}>
     <Button title=' Add New Aspiration' onPress={() => setIsAddMode(true)} />
      <AspirationInput  visible={isAddMode} onAddAspiration={addAspirationHandler}></AspirationInput>
      {/* <View style={styles.inputcontainer}>
        <TextInput placeholder='My Aspiration from this module' style={styles.input} onChangeText={AspirationHandler} value={enteredAspiration}/>
        <Button title='Add' onPress={addAspirationHandler} />
      </View> */}
    {/* <FlatList 
    data={courseAspiration}
    renderItem={itemData=>(
      <View style={styles.listItem}>
      {courseAspiration.map((aspiration)=>
      <Text key={aspiration}>{aspiration}</Text>
      )}
      </View>

    )}>
    </FlatList> */}
    <FlatList
    data={courseAspiration}
    renderItem={itemData=>
    <AspirationItem onDelete={removeApsirationHandler}
    title={itemData.item.value}></AspirationItem>}/>
    </View>
  );
}

const styles = StyleSheet.create({
  Screen:{
    padding:50,
  
  },
  // inputcontainer:{
  //   flexDirection:'row',
  //   justifyContent:'space-between',
  //   alignItems:"center"

  // },
  // input:{
  //   width:'80%',
  //   borderColor:'black',
  //   borderWidth:1,
  //   padding:10,

  // },
  // listItem:{
  //   padding:10,
  //   marginVertical:10,    
  //   backgroundColor:'#ccc',
  //   borderColor:'black',
  //   borderWidth:1,
  // }

});
