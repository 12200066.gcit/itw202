import { StyleSheet, Text, View } from 'react-native';

const Title = ({children}) => {
  return (
       <Text style={styles.title}>{children}</Text>
  )
}
export default Title;

const styles = StyleSheet.create({
    title: {
        fontFamily: 'open-sans-bold',
        // borderWidth: Platform.OS === 'android'? 2 : 0,
        borderWidth: Platform.select({ios:0, android:2}),
        borderColor: 'white',
        textAlign: 'center',
        // fontWeight: 'bold',
        fontSize: 24,
        color:'white',
        padding:12,
        maxWidth:"80%",
        width:300
    }
})