import { StyleSheet, Text, View,Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { StackActions } from '@react-navigation/native';

const Stack =createStackNavigator();
function Screen1({navigation}){
  return(
    <View style={styles.container}>
      <Text>Screen 1</Text>
      <Button
      //  title='Go to third screen'
      //  onPress={()=>{navigation.reset({
      //    index:0,
      //    routes:[{name:'Screen3'}]
      //  })}}
      title='Go to second screen'
      onPress={()=>{navigation.navigate("Screen2")}}
      ></Button>
    </View>
  )
}
function Screen2({navigation}){
  return(
    <View style={styles.container}>
      <Text>Screen2</Text>
      <Button
      // title='Go Back'
      // onPress={()=>{
      //   navigation.goBack();
      // }}
      title='Go to third Screen'
      onPress={()=>{
        navigation.replace('Screen3')
      }}
      >


      </Button>
    </View>
  )
}
function Screen3({navigation}){
  return(
    <View style={styles.container}>
      <Text>Screen3</Text>
      <Button
      title='Go back'
      onPress={()=>{
        navigation.goBack();
      }}
      ></Button>
    </View>
  )
}

export default function App() {
  return (
   <NavigationContainer>
    <Stack.Navigator
      initialRouteName='Screen1'
      screenOptions={{
      headerShown:false,
    }}
    >
    <Stack.Screen  name='Screen1' component={Screen1}/>
    <Stack.Screen  name='Screen2' component={Screen2}/>
    <Stack.Screen  name='Screen3' component={Screen3}/>
    </Stack.Navigator>


   </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
