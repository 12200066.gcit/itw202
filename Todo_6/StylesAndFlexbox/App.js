import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
    <View style={styles.item1}><Text style={styles.textstyle}>1</Text></View>
    <View style={styles.item2}><Text style={styles.textstyle}>2</Text></View>
    <View style={styles.item3}><Text style={styles.textstyle}>3</Text></View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection:"row",
    marginLeft:50,
    marginBottom:500,
    
  },
  item1:{
    backgroundColor:'red',
    height:250,
    width:65,
  },
  item2:{
    backgroundColor:'blue',
    height:250,
    width:150,
  },
  item3:{
    backgroundColor:'green',
    height:250,
    width:10,
  },
  textstyle:{
    textAlign:'center',
    marginTop:100,
  }
});
