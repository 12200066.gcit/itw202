import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import style from './components/style';


// export default function App() {
//   return (
//     <View style={styles.container}>
//       <Text>Open up App.js to start working on your app!</Text>
//       <StatusBar style="auto" />
//     </View>
//   );
// }

//inline styles
// export default function App(){
//   return(
//     <View style = {{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'#34ebd5'}}>
//       <Text style ={{fontSize:20,textAlign:'center',margin:10}}>
//         Welcome to React Native!
//       </Text>
//     </View>
  
//     )
// }
//Stylesheet 
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#222222',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });

//external stylesheet
export default function App() {
  return (
    <View style={styles.container}>
      <Text style={styles.welcome}>Welcome to React Native</Text>
      <StatusBar style="auto" />
    </View>
  );
}