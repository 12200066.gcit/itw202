import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import divide from './components/defaultExport';
import {add,multiply} from './components/nameExport';
import Courses from './components/funcComponent';

export default function App() {
  return (
    <View style={styles.container}>
      <Text>helloworld</Text>
      <Text>Result of addition  {add(4,5)}</Text>
      <Text>Result of multiply {multiply(8,9)}</Text>
      <Text>Result of division {divide(36,6)}</Text>
      <StatusBar style="auto"/>
      <Courses></Courses>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

