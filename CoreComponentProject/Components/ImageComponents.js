import React from 'react';
import {View , Image ,StyleSheet}  from 'react-native';

const MyImagecomponent =()=>{
    return(
        <View>
            <Image style={styles.logocontain} source={require('../assets/favicon.png')}/>
           
        </View>
    )
}
const styles =StyleSheet.create({
    logocontain:{
        width:200,
        height:100,
        resizeMode:'contain',
    },
    logoStretch:{
        width:200,
        height:100,
        resizeMode:'stretch',
    }
})
export default MyImagecomponent;