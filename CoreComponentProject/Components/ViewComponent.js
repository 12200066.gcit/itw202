import React from 'react';
import {Text, View} from 'react-native';
const ViewComponent = () => {
    return (
        <View style={{ flex: 1}}>
            <View style = {{padding:100, backgroundColor:'pink'}}>
                <Text style = {{color: 'red'}}> Text with Background color</Text>
            </View>
        <View style ={{margin:16}} />
        </View>
        );
}

export default ViewComponent;