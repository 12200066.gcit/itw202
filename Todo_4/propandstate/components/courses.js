import react from "react";
import {Text , View } from 'react-native';

const Course =(props) => {
    return(
        <View>
            <Text>Course: {props.CourseCode}</Text>
        </View>
    );
}

const Courses = () => {
    return(
        <View>
        <Course CourseCode ='ITW101' />
        <Course CourseCode ='ITW202'/>
        </View>
    );

}
export default Courses;