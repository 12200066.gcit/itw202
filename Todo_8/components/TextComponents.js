import react from "react";
import {Text ,View,StyleSheet} from 'react-native';


const TextComponent=()=>{
    return(
        <View>
            <Text style={styles.TextStyle}>The  <Text style={styles.bold}>quick brown fox</Text> jumps over the lazy dog</Text>
        </View>
    )
};
const styles=StyleSheet.create({
 bold:{
    fontWeight:'bold',
    fontSize:16,
 },
 TextStyle:{
    fontSize:16,
 },
});
export default TextComponent;
