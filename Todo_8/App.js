import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import myTextComponent from './components/TextComponents';
export default function App() {
  return (
    <View style={styles.container}>
      <TextComponent></TextComponent>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
