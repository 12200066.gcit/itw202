import React from "react";
import {view,StyleSheet ,Text, ViewBase, View} from 'react-native';


const greeting ='Sonam Wangmo';
const greeting1 =<Text>Jigme Wangmo</Text>

const MyComponent = ()=>{
    return (
        <View>
            <Text style={styles.textstyle}>This is a demo of Jsx</Text>
            <Text>Hi there!!!{greeting}</Text>
            {greeting1}
        </View>
    )
};





const styles =StyleSheet.create({
    textstyle: {
        fontSize:35,
    }
});
export default MyComponent;
